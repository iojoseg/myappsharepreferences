package mx.tecnm.misantla.myappsharepreferences

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var red = 0
    var green = 0
    var blue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        seekBarRed.max = 255
        seekBarGreen.max = 255
        seekBarBlue.max = 255

        val sharedPreferences = getSharedPreferences("preferences",0)
        val sharedPreferencesColor = getSharedPreferences("Colores",0)

        btnGuardar.setOnClickListener{
            sharedPreferences.edit().putString("VALOR",edtDatos.text.toString()).apply()
        }

        btnMostrar.setOnClickListener {
            edtDatos.setText(sharedPreferences.getString("VALOR",""))
        }

        seekBarRed.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
              red = progress
                colores(red,green,blue)
                sharedPreferencesColor.edit().putString("red",red.toString()).apply()

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

        seekBarGreen.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                green = progress
                colores(red,green,blue)
                sharedPreferencesColor.edit().putString("green",green.toString()).apply()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

        seekBarBlue.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                blue = progress
                colores(red,green,blue)
                sharedPreferencesColor.edit().putString("blue",blue.toString()).apply()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })

       try{
             var r = sharedPreferencesColor.getString("red","")?.toInt()
            var g = sharedPreferencesColor.getString("green","")?.toInt()
            var b = sharedPreferencesColor.getString("blue","")?.toInt()

            colores(r!!, g!!,b!!)

            seekBarRed.progress = r
            seekBarGreen.progress = g
            seekBarBlue.progress = b
        }catch (ex: Exception){

        }


    }

    fun colores(rojo : Int, verde : Int , azul : Int){
        vista.setBackgroundColor(Color.rgb(rojo,verde,azul))
    }
}

